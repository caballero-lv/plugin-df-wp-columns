(function ($) {
  //
  tinymce.create("tinymce.plugins.DF_Columns", {
    init: function (editor, url) {
      editor.addButton("df_columns", {
        title: "Columns",
        image: url + "/col-thumb.svg",
        onclick: function () {
          if ($("#df_col_box").length == 0) {
            create_box(url);

            $("#df_col_box")
              .on("click", "#col_cancel", function () {
                $("#df_col_box").fadeOut(200);
              })
              .on("click", "#col_insert", function () {
                $("#df_col_box").fadeOut(200, function () {
                  var nodePosition = $(
                    tinyMCE.activeEditor.selection.getNode()
                  ).closest("p, table, h1, h2, h3, ul, ol");
                  var type = $('input[name="select_col_type"]:checked').val();
                  $.get(url + "/../layouts/" + type + ".html", function (data) {
                    //ed.execCommand('mceInsertContent', false, data );
                    //tinymce.execInstanceCommand(ed.id, 'mceInsertContent', false, data );
                    //ed.dom.add(ed.getBody(), 'p', null, data);
                    if ($(nodePosition).parents(".clearfix").length >= 2) {
                      return;
                    }
                    $(data).insertAfter($(nodePosition));
                    var content = $(tinyMCE.activeEditor.getBody());
                    if (content.children("p").first().text().length == 0) {
                      content.children("p").first().remove();
                    }
                  });
                });
              });
          } else {
            $("#df_col_box").css("display", "block");
          }
        },
      });
    },
    createControl: function (n, cm) {
      return null;
    },

    getInfo: function () {
      return {
        longname: "DF WordPress Columns",
        author: "Caballero",
        authorurl: "http://www.caballero.lv/",
        version: "0.3",
      };
    },
  });

  // Register plugin
  tinymce.PluginManager.add("df_columns", tinymce.plugins.DF_Columns);

  function create_box(url) {
    if ($("#df_col_box").length == 0) {
      var box = document.createElement("div");
      box.setAttribute("id", "df_col_box");

      $.ajax({
        url: url + "/../dropdown.php",
        type: "POST",
        data: { url: url },
        success: function (data) {
          box.innerHTML = data;
        },
      });

      // buttons
      var col_insert = document.createElement("span"),
        col_cancel = document.createElement("span");

      col_insert.setAttribute("class", "button-primary");
      col_insert.setAttribute("id", "col_insert");
      col_cancel.setAttribute("class", "button");
      col_cancel.setAttribute("id", "col_cancel");
      col_insert.innerHTML = "Insert";
      col_cancel.innerHTML = "Cancel";

      box.appendChild(col_insert);
      box.appendChild(col_cancel);

      document.getElementById("wpwrap").appendChild(box);
    } else if ($("#df_col_box").css("display") == "none") {
      $("#df_col_box").css("display", "block");
    }
  }
})(jQuery);
