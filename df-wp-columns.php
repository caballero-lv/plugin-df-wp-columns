<?php
/*
Plugin Name: Caballero WordPress Columns
Author: Caballero
Version: 0.3
Author URI: http://www.caballero.lv/
*/

$wp_columns = new DF_Wp_Columns();

class DF_Wp_Columns {

	public $dir;

	public function __construct(){

		$this->dir = plugins_url( '/', __FILE__ );

		add_action( 'admin_enqueue_scripts', array( $this, 'df_col_scripts' ));
		// add_action( 'wp_enqueue_scripts', array( $this, 'df_col_wp_scripts' ));
		add_action( 'mce_css', array( $this, 'df_mce_styles' ) );
		add_action( 'init', array( $this, 'df_add_col_btn' ));
		// add_filter( 'tiny_mce_version', array( $this, 'my_refresh_mce' ) );
	}

	public function df_col_wp_scripts(){

		wp_enqueue_script( 'jquery-ui-sortable' );
		wp_enqueue_style( 'df_col_wp_style', $this->dir . 'css/df_col_wp_styles.css' );
	}

	public function df_col_scripts(){

		wp_enqueue_style( 'df_col_style', $this->dir . 'css/df_col_styles.css' );
	}

	public function df_mce_styles( $mce_css ){

		if ( ! empty( $mce_css ) ){
			$mce_css .= ',';
		}

		$mce_css .= $this->dir . 'css/df_editor_styles.css';

		return $mce_css;
	}

	public function df_add_col_btn(){

		if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ){
			return;
		}

		if ( get_user_option('rich_editing') == 'true') {

			add_filter( 'mce_external_plugins', array( $this, 'df_add_col_tinymce_plugin' ), 99 );
			add_filter( 'mce_buttons', array( $this, 'df_register_col_btn' ), 99 );
		}
	}

	function df_add_col_tinymce_plugin($plugin_array) {

		$plugin_array['df_columns'] = $this->dir . 'js/df_col_scripts.js';
		return $plugin_array;
	}

	function df_register_col_btn($buttons) {

		array_push( $buttons, "df_columns" );

		return $buttons;
	}

	function my_refresh_mce($ver) {
	  $ver += 3;
	  return $ver;
	}
}