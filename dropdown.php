<?php
$dir = $_POST['url'] . '/../'; ?>
<h3>Select layout type</h3>

<div class="col-wrap clearfix">
	<label for="col_type_1">
		<img src="<?php echo $dir; ?>img/1column.png" /><br />
		1 column<br />
		<input type="radio" id="col_type_1" name="select_col_type" value="1" />
	</label>

	<label for="col_type_2-2">
		<img src="<?php echo $dir; ?>img/2column.png" /><br />
		2 columns<br />
		<input type="radio" id="col_type_2-2" name="select_col_type" value="2-2" />
	</label>

	<label for="col_type_1-2">
		<img src="<?php echo $dir; ?>img/1-2column.png" /><br />
		1:2 columns<br />
		<input type="radio" id="col_type_1-2" name="select_col_type" value="2-1" />
	</label>

	<label for="col_type_2-1">
		<img src="<?php echo $dir; ?>img/2-1column.png" /><br />
		2:1 columns<br />
		<input type="radio" id="col_type_2-1" name="select_col_type" value="1-2" />
	</label>

	<label for="col_type_1-3">
	<img src="<?php echo $dir; ?>img/1-3column.png" /><br />
	1:3 columns<br />
	<input type="radio" id="col_type_1-3" name="select_col_type" value="3-1" />
	</label>

	<label for="col_type_3-1">
		<img src="<?php echo $dir; ?>img/3-1column.png" /><br />
		3:1 columns<br />
		<input type="radio" id="col_type_3-1" name="select_col_type" value="1-3" />
	</label>

	<label for="col_type_3-3-3">
		<img src="<?php echo $dir; ?>img/3column.png" /><br />
		3 columns<br />
		<input type="radio" id="col_type_3-3-3" name="select_col_type" value="1-1-1" />
	</label>
	<label for="col_type_4">
		<img src="<?php echo $dir; ?>img/4column.png" /><br />
		4 columns<br />
		<input type="radio" id="col_type_4" name="select_col_type" value="4" />
	</label>
</div>

<span class="button-primary" id="col_insert" value="Insert">Insert</span>
<span class="button" id="col_cancel" value="Cancel">Cancel</span>